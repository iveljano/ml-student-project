# ml-student-project

Machine learning project where students can learn basic concepts of classification problems.

## Dataset

In the dataset, there are 80000 images of the jets. 
The size of one image is 100x100.
There are 5 classes: quark, gluon, W, Z, top.
The labels are represented as one-hot encoded.
[1, 0, 0, 0, 0] gluons
[0, 1, 0, 0, 0] quarks
[0, 0, 1, 0, 0] Ws
[0, 0, 0, 1, 0] Zs
[0, 0, 0, 0, 1] tops