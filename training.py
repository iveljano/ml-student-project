import tensorflow as tf
from dataset import load_tfrecord


if __name__ == "__main__":

    training_paths = ["/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_1-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_2-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_3-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_4-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_5-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_6-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_7-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_8-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_9-of-13.tfrecord",]

    validation_paths = ["/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_10-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_11-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_12-of-13.tfrecord",
        "/eos/user/d/dgolubov/Public/jet_tfrecords/jet_data_13-of-13.tfrecord"]


    model = tf.keras.Sequential(
            [
                tf.keras.layers.Input(shape=(100, 100, 1)),
                tf.keras.layers.Conv2D(
                    kernel_size=(5, 5),
                    filters=5,
                    strides=(1, 1),
                    padding="same",
                    data_format="channels_last",
                ),
                tf.keras.layers.BatchNormalization(),
                tf.keras.layers.Activation("relu"),
                tf.keras.layers.MaxPooling2D(pool_size=(5, 5)),
                tf.keras.layers.Dropout(0.25),
                tf.keras.layers.Conv2D(
                    kernel_size=(3, 3), filters=3, strides=(1, 1), padding="same"
                ),
                tf.keras.layers.BatchNormalization(),
                tf.keras.layers.Activation("relu"),
                tf.keras.layers.MaxPooling2D(pool_size=(3, 3)),
                tf.keras.layers.Dropout(0.25),
                tf.keras.layers.Flatten(),
                tf.keras.layers.Dense(5, "relu"),
                tf.keras.layers.Dense(5, "softmax"),
            ]
        )

    model.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
        metrics=tf.keras.metrics.CategoricalAccuracy(),
    )

    batch_size = 8

    model.fit(
        load_tfrecord(training_paths, batch_size),
        epochs=3,
        validation_data=load_tfrecord(validation_paths, batch_size),
        callbacks=None,
    )