## How to run

### Create a PVC
`kubectl -n PERSONAL_NAMESPACE apply -f pvc.yaml`

### Create a notebook server with the following image
`dejangolubovic/hssip-2022-ml-pipeline`

### In Terminal get this repo
`git clone https://gitlab.cern.ch/iveljano/ml-student-project.git`

### Run pipeline by following https://ml.docs.cern.ch/pipelines/#mnist-notebook
Use pipeline/pipeline-notebook.ipynb from this repo
